pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Buyer.sol";

contract TestBuyer {
  // The address of the Buyers contract to be tested
  Buyer Buyers = Buyer(DeployedAddresses.Buyers());

  // The id of the Item that will be used for testing
  uint expectedItemid = 8;

  //The expected user buy item is this contract
  address expectedBuyer = address(this);

  // Testing the buy() function
  function testUserCanbuyItem() public {
    uint returnedId = Buyer.buy(expectedItemid);

    Assert.equal(returnedId, expectedItemid, "Buyer of the expected Item should match what is returned.");
  }

  function testGetbuyerAddressByItemId() public {
   address buyer = Buyer.buyers(expectedItemid);

   Assert.equal(buyer, expectedbuyer, "Owner of the expected Item should be this contract");
  }

  // Testing retrieval of all buyer 
  function testGetbuyerAddressByItemIdInArray() public {
   // Store buyers in memory rather than contract's storage
   address[16] memory buyers = Buyer.getbuyers();

   Assert.equal(buyers[expectedItemid], expectedbuyer, "Buyer of the expected item should be this contract");
  }
}