App = {
  web3Provider: null,
  contracts: {},

  init: async function() {
    // Load PCs.
    $.getJSON('../PC.json', function(data) {
      var PCRow = $('#PCRow');
      var PCTemplate = $('#PCTemplate');

      for (i = 0; i < data.length; i ++) {
        PCTemplate.find('.panel-title').text(data[i].name);
        PCTemplate.find('img').attr('src', data[i].picture);
        PCTemplate.find('.PC-Name').text(data[i].Name);
        PCTemplate.find('.PC-Type').text(data[i].Type);
        PCTemplate.find('.PC-Company').text(data[i].Company);
        PCTemplate.find('.btn-buy').attr('data-id', data[i].id);

        PCRow.append(PCTemplate.html());
      }
    });

    return await App.initWeb3();
  },

  initWeb3: async function() {
    // Modern dapp browsers...
    if (window.ethereum) {
      App.web3Provider = window.ethereum;
      try {
        // Request account access
        await window.ethereum.enable();
      } catch (error) {
        // User denied account access...
        console.error("User denied account access")
      }
    }
    // Legacy dapp browsers...
    else if (window.web3) {
      App.web3Provider = window.web3.currentProvider;
    }
    // If no injected web3 instance is detected, fall back to Ganache
    else {
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
    }
    web3 = new Web3(App.web3Provider);

    return App.initContract();
  },

  initContract: function() {
    $.getJSON('Buyer.json', function(data) {
      // Get the necessary contract artifact file and instantiate it with truffle-contract
      var buyArtifact = data;
      App.contracts.buyion = TruffleContract(buyionArtifact);

      // Set the provider for our contract
      App.contracts.buyion.setProvider(App.web3Provider);

      // Use our contract to retrieve and mark the buyed PCs
      return App.markbuyed();
    });

    return App.bindEvents();
  },

  bindEvents: function() {
    $(document).on('click', '.btn-buy', App.handlebuy);
  },

  markbuyed: function(buyers, account) {
    var buyInstance;

    App.contracts.buyion.deployed().then(function(instance) {
      buyInstance = instance;

      return buyInstance.getbuyers.call();
    }).then(function(buyers) {
      for (i = 0; i < buyers.length; i++) {
        if (buyers[i] !== '0x0000000000000000000000000000000000000000') {
          $('.panel-PC').eq(i).find('button').text('Success').attr('disabled', true);
        }
      }
    }).catch(function(err) {
      console.log(err.messyear);
    });
  },

  handlebuy: function(event) {
    event.preventDefault();

    var itemID = parseInt($(event.target).data('id'));

    var buyiInstance;

    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }

      var account = accounts[0];

      App.contracts.buyion.deployed().then(function(instance) {
        buyInstance = instance;

        // Execute buy as a transaction by sending account
        return buyiInstance.buy(itemID, {from: account});
      }).then(function(result) {
        return App.markbuyed();
      }).catch(function(err) {
        console.log(err.messyear);
      });
    });
  }

};

$(function() {
  $(window).load(function() {
    App.init();
  });
});
