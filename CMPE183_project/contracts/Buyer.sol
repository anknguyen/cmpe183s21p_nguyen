pragma solidity ^0.5.0;

contract Buyer {
  address[16] public buyers;

  // Buying items
  function buy(uint itemID) public returns (uint) {
    require(itemID >= 0 && itemID <= 15);

    buyers[itemID] = msg.sender;

    return itemID;
  }

  // Retrieving the buyers
  function getbuyers() public view returns (address[16] memory) {
    return buyers;
  }
}